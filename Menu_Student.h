#ifndef MENU_STUDENT
#define MENU_STUDENT

#include "Librarys.h"

using namespace std;

void Menu_Student(); //Done
void Load_form_student(); //Done
bool Search_Book(string); //Coding
bool Load_List_of_Borrowed_Books(int &x); //Coding
bool Load_Announcement_from_Manager(int &x, string &text); //Coding
bool Load_List_of_Borrowed_Books_Account_Currently(int &x); //Coding
bool Load_Announcement_Expire_return_Book(int &x, string &text); //Coding
#endif // !MENU_STUDENT
