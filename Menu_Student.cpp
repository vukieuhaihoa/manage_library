#include "Menu_Student.h"
#include <iomanip>

#pragma warning(disable:4996)

extern map<string, Infor_Account> List_Infor_Account;
extern Infor_Account Account_Curren;
extern map<string, Infor_Book> List_Infor_Books;
extern Infor_User_Borrowed List_Book_Borrowed[100];
extern Infor_User_Borrowed List_Book_Borrowed_Acc_Currently[6];
extern int Book_Borrowed;
extern int Book_Borrowed_of_Acc_Currently;
extern int count_announce1;
extern int count_announce2;
extern string text;
using namespace std;


const int monthDays[12] = { 31, 28, 31, 30, 31, 30,31, 31, 30, 31, 30, 31 };
ostream& operator<< (ostream &output, const Date &temp)
{
	output << temp.Day << "/" <<  temp.Month <<  "/" << temp.Year;
	return output;
}

ostream& operator<<(ostream &output, const Infor_User_Borrowed infor)
{
	output << setw(8) << infor.Id_people << "|" << setw(5) << infor.Id_Book << "|"<< setw(28) << infor.Name_Book << "|" << setw(8) << infor.Date_From << "|" << setw(8) << infor.Date_To << "|" << setw(30) << infor.Note << "|";
	return output;
}

int countLeapYears(Date d)
{
	int years = d.Year;

	// Check if the current year needs to be considered 
	// for the count of leap years or not 
	if (d.Month <= 2)
		years--;

	// An year is a leap year if it is a multiple of 4, 
	// multiple of 400 and not a multiple of 100. 
	return years / 4 - years / 100 + years / 400;
}

int getDifference(Date dt1, Date dt2)
{
	// COUNT TOTAL NUMBER OF DAYS BEFORE FIRST DATE 'dt1' 

	// initialize count using years and day 
	long int n1 = dt1.Year * 365 + dt1.Day;

	// Add days for months in given date 
	for (int i = 0; i<dt1.Month - 1; i++)
		n1 += monthDays[i];

	// Since every leap year is of 366 days, 
	// Add a day for every leap year 
	n1 += countLeapYears(dt1);

	// SIMILARLY, COUNT TOTAL NUMBER OF DAYS BEFORE 'dt2' 

	long int n2 = dt2.Year * 365 + dt2.Day;
	for (int i = 0; i<dt2.Month - 1; i++)
		n2 += monthDays[i];
	n2 += countLeapYears(dt2);

	// return difference between two counts 
	return (n2 - n1);
}

void Menu_Student()
{
	int choose;
	string temp = "";
	while (true)
	{
		system("cls");
		Load_form_student();
		cout << "Cac chuc nang danh cho sinh vien:\n";
		cout << "1.Truy cap kho sach cua thu vien\n"; //Codingcou
		cout << "2.Danh sach nhung sach da muon\n"; //Coding
		cout << "3.Thong bao("  << count_announce1 + count_announce2 << ")\n"; //Coding
		cout << "4.Xem thong tin ca nhan\n"; //Done
		cout << "5.Thay doi thong tin ca nhan\n"; // Done
		cout << "6.Thay doi mat khau\n"; // Done
		cout << "7.Thoat ung dung\n"; // Done
		cout << "Moi ban chon chuc nang:";
		cin >> choose;
		if (choose == 1)
		{
			cin.ignore();
			while (true)
			{
				system("cls");
				Load_form_ListBook();
				Set_Color(7);
				cout << "Moi ban chon cac tinh nang sau:\n";
				cout << "1.Tai toan bo danh sach cac sach co trong thu vien\n"; //Done
				cout << "2.Tim sach theo ten\n";// chi tim dung nhu ten (ke ca hoa thuong) => se fix sau
				cout << "3.Quay lai menu ban dau\n"; //Done
				cout << "Moi ban nhap:";
				int choo = 0;
				cin >> choo;
				cin.ignore();
				if (choo == 1)
				{
					system("cls");
					Print_List_Book();
					cout << "ENTER de quay lai Menu truoc!";
					getline(cin, temp);
				}
				else if (choo == 2)
				{
					system("cls");
					cout << "Searching...\n";
					cout << "Moi ban nhap vao ten hoac mot phan ten cua sach can tim:";
					string Name_of_Book = "";
					getline(cin, Name_of_Book);
					bool check_exist = Search_Book(Name_of_Book);
					if (check_exist)
					{
						cout << "ENTER de quay lai Menu truoc!";
						getline(cin, temp);
					}
					else
					{
						cout << "Noi dung khong tim thay :(\n";
						cout << "ENTER de quay lai Menu truoc!";
						getline(cin, temp);
					}

				}
				else if (choo == 3)
				{
					break;
				}
			}
		}
		else if (choose == 2)
		{
			cin.ignore();
			system("cls");
			cout << "DANH SACH NHUNG SACH DA MUON LA:\n";
			for (int i = 0; i < Book_Borrowed_of_Acc_Currently; i++)
			{
				cout << setw(3) << i + 1 << "|" << List_Book_Borrowed_Acc_Currently[i] << "\n";
			}
			cout << "Enter de quay lai man hinh chinh\n";
			getline(cin, temp);
		}
		else if (choose == 3)
		{
			cin.ignore();
			system("cls");
			cout << text;
			cout << "Enter de quay lai man hinh chinh\n";
			getline(cin, temp);
		}
		else if (choose == 4)
		{
			cin.ignore();
			Show_Infor();
		}
		else if (choose == 5)
		{
			cin.ignore();
			bool check = Update_Account();
			system("cls");
			if (check)
			{
				cout << "CAP NHAT THONG TIN THANH CONG\n";
				cout << "Enter de quay lai man hinh chinh\n";
				getline(cin, temp);

			}
			else
			{
				cout << "CAP NHAT THONG TIN THAT BAI\n";
				return;
			}
		}
		else if (choose == 6)
		{
			cin.ignore();
			bool check = Change_PassWord();
			system("cls");
			if (check)
			{
				cout << "CAP NHAT MAT KHAU THANH CONG\n";
				cout << "Enter de quay lai man hinh chinh\n";
				getline(cin, temp);

			}
			else
			{
				cout << "CAP NHAT MAT KHAU THAT BAI\n";
				return;
			}
		}
		else if(choose == 7)
		{
			return;
		}
	}
	
}

void Load_form_student()
{
	ifstream Load_file("DataBase/LogoMenuStudent.txt", ios::in);
	Set_Color(12);
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		Load_file.close();
		return;
	}
	string str_temp = "";
	while (!Load_file.eof())
	{
		str_temp = "";
		getline(Load_file, str_temp);
		cout << "\n";
		cout << str_temp;
	}
	Load_file.close();
	Set_Color(7);
}

bool Search_Book(string keyword)
{
	map<string, Infor_Book> result;
	bool check = false;
	int position_first = -1;
	for (auto& x: List_Infor_Books)
	{
		position_first = x.first.find(keyword);
		if (position_first != -1)
		{
			check = true;
			result[x.first] = x.second;
		}
	}
	if (check == true)
	{
		cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
		cout << "|   ID   |            Name            | Price | AMT |      Author      |                   Describes                 |\n";
		cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
		cout << fixed << setfill(' ');
		for (auto& x : result)
		{
			cout << "|" << setw(8) << x.second.Id << "|" << setw(28) << x.second.Name << "|" << setw(7) << setfill(' ') << x.second.Price << "|" << setw(5) << x.second.Amount << "|" << setw(18) << x.second.Author << "|" << setw(45) << x.second.Describe << "|\n";
			cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
		}
	}
	return check;
}

bool Load_List_of_Borrowed_Books(int &x) // x la so cuon sach da muon
{
	string temp = "";
	string str[10];
	int i = 0;
	int j = 0;
	int count = 0;
	string ss[10];

	// moi file load du lieu len
	ifstream Load_file("DataBase/Borrowed_Book.txt", ios::in);
	if (!Load_file)
	{
		return false;
	}
	while (!Load_file.eof())
	{
		Infor_User_Borrowed clone;
		temp = "";
		getline(Load_file, temp);
		istringstream istr(temp);
		i = 0;
		while (getline(istr, str[i++], '|')){}
		
		clone.Id_people = str[0];
		clone.Id_Book = str[1];
		clone.Name_Book = str[2];
		
		istringstream temp1(str[3]);
		j = 0;
		while (getline(temp1, ss[j++], '/')){}
		clone.Date_From.Day = stoi(ss[0]);
		clone.Date_From.Month = stoi(ss[1]);
		clone.Date_From.Year = stoi(ss[2]);
		
		istringstream temp2(str[4]);
		j = 0;
		while (getline(temp2, ss[j++], '/')){}
		clone.Date_To.Day = stoi(ss[0]);
		clone.Date_To.Month = stoi(ss[1]);
		clone.Date_To.Year = stoi(ss[2]);

		clone.Note = str[5];
		List_Book_Borrowed[count++] = clone;
	}
	x = count - 1;
	Load_file.close();
	return true;
}

bool Load_List_of_Borrowed_Books_Account_Currently(int &x) // x la so cuon sach da muon
{
	x = 0;
	for (int i = 0; i < Book_Borrowed; i++)
	{
		if (List_Book_Borrowed[i].Id_people == Account_Curren.Id)
		{
			List_Book_Borrowed_Acc_Currently[x++] = List_Book_Borrowed[i];
		}
	}
	x;
	return true;
}

bool Load_Announcement_from_Manager(int &x, string &text)
{
	string temp = "";
	x = 0;
	ifstream Load_file("DataBase/Announce.txt", ios::in);
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		return false;
	}
	while (!Load_file.eof())
	{
		temp = "";
		getline(Load_file, temp);
		if (temp == "")
		{
			break;
		}
		text += temp + "\n";
		x++;
	}
	Load_file.close();
	return true;
}

bool Load_Announcement_Expire_return_Book(int &x, string &text)
{
	Date Hom_Nay;
	x = 0;
	GetTime_System(Hom_Nay);
	int n;
	for (int i = 0; i < Book_Borrowed_of_Acc_Currently; i++)
	{
		n = getDifference(Hom_Nay, List_Book_Borrowed_Acc_Currently[i].Date_To);
		if (n == 2)
		{
			text += "Con 2 ngay nua la den han tra sach: " + List_Book_Borrowed_Acc_Currently[i].Name_Book + "\n";
			x++;
		}
		if (n < 0)
		{
			text += "Ban da qua han tra sach: " + List_Book_Borrowed_Acc_Currently[i].Name_Book + " : " + to_string(-n) + " ngay. Neu khong tra se bi tru tien tinh theo ngay\n";
			x++;
		}
	}
	return true;
}
